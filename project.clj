(defproject indexme "1.0.0-SNAPSHOT"
  :description "Generates an index given a list of words and a document.  Currently supports PDF."
  :licence "Eclipse Public Licence 1.0"
  :main indexme.core
  :dependencies [
                 [org.clojure/clojure "1.3.0"]
                 [storm/tools.cli "0.2.2"]
                 ])