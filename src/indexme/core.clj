(ns indexme.core
  (:gen-class :main true)

  )

  (use '[clojure.java.shell :only [sh]])
  (use '[clojure.tools.cli  :only [cli]])
  (use 'clojure.set)
;; Multiformat support, returns a string containing the filetype.
(defn get-filetype [file]
  (let [fout (sh "file" "--mime-type" file)]
       (.trim (get (.split (get fout :out) ":") 1)))
       )

;; PDF Implementation
(defn pdf-getpagetext [file pagen]
  (let [x (sh "pdftotext" file "-f" (format "%d" pagen) "-l" (format "%d" pagen) "-")] (if (= (get x :exit) 0) (get x :out) nil) ))

;; Parses the output of PDFINFO to obtain the number of pages.
(defn pdf-getpagecount [file]
  (Integer/parseInt ;; Change it into an integer
   (.trim  ;; Remove leading whitespace
    (get 
     (.split 
      (first (filter (fn [x] (= (.trim (get (.split x ":") 0)) "Pages"))
                     (.split (get (sh "pdfinfo" file) :out) "\n") )) ":") 1) ) ))
 


;; index a page:  Traverses the set of words, if it is found within the text, it adds a map containing the word, and the page number in a singleton set to the list, otherwise, it just adds an empty map.  It can return a list of empty sets, but never nil.

(defn index-page [word-set page-text pagen]
  (reduce (fn [map1 map2] (merge-with union map1 map2)) (map (fn [word] (if (not (= (.indexOf (.toUpperCase page-text) (.toUpperCase word)) -1)) (sorted-map word (sorted-set pagen)) {})) word-set)))


;; This is the old implemenation, it has been replaced with map/reduce
;; The recursive helper funciton.
;(defn index-document-1 [file page-function page-count word-set pagen]
;  (if (< pagen page-count)
;    (merge-with union
;                (index-page word-set (page-function file pagen) pagen)
;                (index-document-1 file page-function page-count word-set (+ pagen 1)))
;    (index-page word-set (page-function file pagen) pagen)))
  



;; Reads the index file into a sorted set of the words to index; this also removes all the leading whitespace.  It first creates a list of singleton sets, then reduces the singleton sets by unioning them into a large set.
(defn 
   read-index-file [file]
  (reduce (fn [x y] (union x y))
          (map (fn [x] (sorted-set (.trim x))) (.split (slurp file) "\n")))
  )

;; Indexes a document, takes functions to get the page as text, and to get the page count.
(defn index-document [file page-function page-count-function word-set]
  (reduce (fn [x y] (merge-with union x y) ) (map (fn [x] (index-page word-set (page-function file x) x)) (range 1 (+ (page-count-function file) 1)))))



(defn ignoring-case [s1 s2]
  (.compareToIgnoreCase (key s1) (key s2)))

(defn index-to-string [index-set]
(reduce
 (fn [x y] (if (not (= (first (.toUpperCase x)) (first (.toUpperCase y)))) (format "%s\n\n%s" x y) (format "%s\n%s" x y)))
 (map (fn [x] (format "%s %s" (get x 0)
                              (reduce (fn [x1 y] (format "%s,%s" x1 y)) (get x 1)
                              )))
              (sort ignoring-case index-set))))


(defn -main [& args]


;; For future support of different filetypes, when proper toolage is added.  Right now only PDF is supported.

(def filetype-support (hash-map "application/pdf" [pdf-getpagetext pdf-getpagecount]))

(println (count args))

;; Display the USAGE message if there are no command line arguments.
(if (or (= args nil) (< (count args) 2))
  (do
    (println "USAGE: indexme <file> <index-file>")
    (println "Where <file> is one of the supported filetypes, and, <index-file> is a list of words separated by newlines.")
    (println "Supported file (MIME) types:")
    (println (reduce (fn [x y] (format "%s\n%s" x y)) (keys filetype-support)))
    (System/exit 0)
    ))

(def cli-result (cli args ["--[no-]html" :default false] ["--[no-]skiplastpage" :default false]))

;; Display an error if the filetype is not supported.
(if (not (.contains (keys filetype-support) (get-filetype (first args))))
    (do
      (println "Filetype not supported.")
      (println "Supported file (MIME) types:")
      (println (reduce (fn [x y] (format "%s\n%s" x y)) (keys filetype-support)))

	(System/exit 1)
	))

;;; Retrieve the appropriate function from the filetype support map.
(let [funs (get filetype-support (get-filetype (first args)))]
(def index-set (index-document (first (first (rest cli-result))) (get funs 0) (if (get (first cli-result) :skiplastpage) (fn [x] (- ((get funs 1) x) 1)) (get funs 1)) (read-index-file (first (rest args))))))



;;;Display the index
(println "Index")
(println "===========")
(if (= index-set {}) (do
                       (println (format "None of the words in the word list were present in %s" (first args)))
                       (System/exit 0)
                       ))
(println (index-to-string index-set))

;(println (reduce
; (fn [x y] (if (not (= (first (.toUpperCase x)) (first (.toUpperCase y)))) (format "%s\n\n%s" x y) (format "%s\n%s" x y)))
; (map (fn [x] (format "%s %s" (get x 0)
 ;                             (reduce (fn [x1 y] (format "%s,%s" x1 y)) (get x 1)
  ;                            )))
   ;           (sort ignoring-case index-set))))

(System/exit 0))